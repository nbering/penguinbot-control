import asyncio
import PySimpleGUI as sg
from bleak import BleakClient

# TODO: Set address
address = "XX:XX:XX:XX:XX:XX"

WRITE_SERIAL_UUID = "0000ffe2-0000-1000-8000-00805f9b34fb"

COMMANDS = {
    "Forward": 0x66,
    "Back": 0x62,
    "Left": 0x6C,
    "Right": 0x69,
    "Stop": 0x73,
    "Music": 0x31,
    "Dance": 0x32,
    "Avoid": 0x33,
    "Vol+": 0x34,
    "Vol-": 0x35,
    "Follow": 0x36,
    "RR+": 0x37,
    "RL+": 0x38,
    "YR+": 0x39,
    "YL+": 0x30,
    "RR-": 0x61,
    "RL-": 0x63,
    "YR-": 0x64,
    "YL-": 0x65
}

class PenguinClient(object):
    def __init__(self, address):
        self._client = BleakClient(address)
        self.is_busy = False
    
    def __del__(self):
        loop.create_task(self._client.disconnect())

    async def connect(self):
        await self._client.connect()

    def is_connected(self):
        return self._client.is_connected

    async def send_command(self, command):
        self._is_busy = True
        await self._client.write_gatt_char(WRITE_SERIAL_UUID, data=[COMMANDS[command]])
        self.is_busy = False


# AsyncIO utility function to run through the event loop once
def run_once(loop):
    loop.call_soon(loop.stop)
    loop.run_forever()


def main():
    layout = [
        [sg.Push(), sg.B("Forward"), sg.Push()],
        [sg.B("Left"), sg.B("Stop"), sg.B("Right")],
        [sg.Push(), sg.B("Back"), sg.Push()],
        [sg.HorizontalSeparator()],
        [sg.B("Vol+"), sg.B("Vol-")],
        [sg.B("Dance"), sg.B("Follow")],
        [sg.B("Music"), sg.B("Avoid")],
        [sg.HorizontalSeparator()],
        [sg.B("RR-"), sg.B("RR+")],
        [sg.B("RL-"), sg.B("RL+")],
        [sg.B("YR-"), sg.B("YR+")],
        [sg.B("YL-"), sg.B("YL+")],
        [sg.HorizontalSeparator()],
        [sg.B("Connect")],
        [sg.Text(key="-TASKS-")],
        [sg.Text(key="-CONNECTED-")]

    ]

    main_window = sg.Window("Penguin Control", layout, finalize=True)

    penguin = PenguinClient(address)
    loop.create_task(penguin.connect())
    
    while True:
        window, event, values = sg.read_all_windows(timeout=1)
        if event == sg.WIN_CLOSED or event == 'Exit':
            break

        if event == "Connect":
            loop.create_task(penguin.connect())

        if event in COMMANDS:
            loop.create_task(penguin.send_command(event))

        ntasks = len(asyncio.all_tasks(loop))
        main_window["-TASKS-"].update("Running Tasks: {}".format(ntasks))
        main_window["-CONNECTED-"].update("Connected: {}".format(penguin.is_connected()))

        for cmd in COMMANDS.keys():
            main_window[cmd].update(disabled=(not penguin.is_connected() or penguin.is_busy))
        run_once(loop)

    window.close()

if __name__ == "__main__":
    loop = asyncio.get_event_loop()

    main()

    # Async Cleanup
    while len(asyncio.all_tasks(loop)):
        run_once(loop)
    asyncio.run(loop.shutdown_asyncgens())
    loop.close()
